import { React } from 'react';
import './Loading.scss';
import Logo from '../../assets/images/logo.png';
const Loading=()=>(
    <>
        <div className="loader-wrapper">
            <div className="loader">
                <div className="loader__bar"></div>
                <div className="loader__bar"></div>
                <div className="loader__bar"></div>
                <div className="loader__bar"></div>
                <div className="loader__bar"></div>
                <div className="loader__ball"></div>
            </div>
        </div>
    </>
);

export default Loading;

