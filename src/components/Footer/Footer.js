/* ReactStrap */
import { Container, Row, Col, Button, Card, Image } from 'react-bootstrap';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    NavLink as Link
  } from "react-router-dom";

/* Components */
import Logo from '../../assets/images/logo.png';
import Twitter_Icon from '../../assets/icons/twitter.svg';
import Facebook_Icon from '../../assets/icons/facebook.svg';
import Google_Icon from '../../assets/icons/google.svg';

import './Footer.scss';

function Footer(props) {
    return (
        <div className="footer">
            <Container>
                <Row>
                    <Col md={4}>
                        <Image src={Logo} className="footer-logo" />
                        <span className="footer-address">
                            Số 5, Ngõ 128 Phố Vọng, Phương Liệt, Thanh Xuân, Hà Nội, Việt Nam.
                        </span>
                        <span className="footer-times">
                            <b>Thứ Hai – Thứ Bảy:</b>
                            <br/>
                            10:00 – 18:00 GMT+6
                        </span>
                        <div className="footer-socials">
                            <ul>
                                <li>
                                    <a>
                                        <img src={Twitter_Icon} />
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <img src={Facebook_Icon} />
                                    </a>
                                </li>
                                <li>
                                    <a>
                                        <img src={Google_Icon} />
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </Col>
                    <Col md={1}></Col>
                    <Col md={4}>
                        <span className="footer-title">DANH MỤC</span>
                        <ul className="footer-list">
                            <li>
                                    <Link 
                                        className="nav-link" 
                                        to="/about_us"
                                    >
                                        Giới thiệu
                                    </Link>
                                
                            </li>
                            <li>
                                <Link 
                                    className="nav-link" 
                                    to="/service_us"
                                >
                                    Giải pháp
                                </Link>
                            </li>
                            <li>
                                <Link 
                                    className="nav-link" 
                                    to="/customers"
                                >
                                    Khách hàng
                                </Link>
                            </li>
                            <li>
                                <Link 
                                    className="nav-link" 
                                    to="/contact_us"
                                >
                                    Liên hệ
                                </Link>
                            </li>
                        </ul>
                    </Col>
                    <Col md={3}>
                        <span className="footer-title">LIÊN HỆ (SALES)</span>
                        <ul className="footer-list footer-list-contact">
                            <li>
                                <a href="mailto:contact@iwannatech.com">
                                    <span>contact@iwannatech.com</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <span className="font-weight-bold">(+84) (0)37 701 2386 (Zalo)</span>
                                </a>
                            </li>
                            <li>
                                <a>
                                    <span className="font-12">THỜI GIAN:: 10:00–18:00 GMT+6</span>
                                </a>
                            </li>
                        </ul>
                    </Col>
                    <Col md={12}>
                        <div className="is-line"></div>
                        <span className="footer-absolute">All Rights Reserved © 2020 by <a href="#">iwannatech.com</a></span>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default Footer;
