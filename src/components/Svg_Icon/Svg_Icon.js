import React from 'react'

import './Svg_Icon.scss';

const SvgIcon = ({ img, size, children, data_aos }) => (
    <>
        <div 
            className={data_aos ? 'SvgIcon_box aos-init aos-animate': 'SvgIcon_box'}
            data-aos={data_aos}
            data-aos-delay="1000"
            data-aos-duration="1000"
            data-aos-easing="ease-in-out"
        >
            <img src={img} style={{ width: `${size}`, height:`${size}` }} />
            <div 
                data-aos={data_aos}
                data-aos-delay="1000"
                data-aos-duration="1000"
                data-aos-easing="ease-in-out"
            >
                {children}
            </div>
        </div> 
    </>
)

export default SvgIcon;