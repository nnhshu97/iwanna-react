import './Card.scss';

function Card_Post(props) {
    return (
        <a className="card_post">
            <h3 className="card_post__title">{props.post.title}</h3>
            <div className="card_post__img">
                <img src={props.post.img} width="100%" />
            </div>
            <div className="card_post__content">{props.post.content}</div>
            <div className="card_post__authorDate">{props.post.author}</div>
        </a>
    );
}

export default Card_Post;
