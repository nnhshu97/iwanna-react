import React,{useEffect} from 'react';
import { AiOutlineCaretDown, AiOutlineBars, AiOutlineClose } from "react-icons/ai";

import {
    BrowserRouter as Router,
    Switch,
    Route,
    NavLink as Link
  } from "react-router-dom";
import { Container, Row, Col, Nav } from 'react-bootstrap';
import Logo from '../../assets/images/logo.png';
import './Header.scss';

function Header() {
    const [scrolled,setScrolled] = React.useState(false);
    const [expanded,isExpanded] = React.useState(false);
    const [show, setShow] = React.useState(false);
    const showDropdown = (e)=>{
        setShow(!show);
    }
    const hideDropdown = e => {
        setShow(false);
    }
    const handleScroll = () => {
        const offset=window.scrollY;
        if(offset > 200 ){
            setScrolled(true);
        }
        else{
            setScrolled(false);
        }
    }

    /* Nav mobile */
    const handleToggle = (e) => {
        e.preventDefault();
        isExpanded(expanded => !expanded);
    }
    useEffect(() => {
        window.addEventListener('scroll',handleScroll)
    })

    let navbarClasses=['header-wrapper'];
    if(scrolled){
        navbarClasses.push('scrolled');
    }
    return (
        <div className={navbarClasses.join(" ")}>
            <Container>
                <Row>
                    <Col md={4}>
                        <div className="header-left">
                            <Link to="/" className="logo">
                                <img src={Logo} width="100px" />
                            </Link>
                            <div className="menu-icon" onClick={ (e) => isExpanded( expanded => !expanded )}>
                                { 
                                    expanded ? <AiOutlineClose  size="24"  /> : <AiOutlineBars size="24"  />
                                }
                            </div>
                        </div>
                    </Col>
                    <Col md={8}>
                        <div className={ expanded ? 'navbars navbars-mobile' : 'navbars' }>
                            <Nav className="justify-content-end">
                                <Nav.Item>
                                    <Link 
                                        className="nav-link" 
                                        to="/about_us"
                                        activeClassName="nav-link--active"
                                    >
                                        Giới thiệu
                                    </Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <div className={show === true ? "dropdown dropdown-show": "dropdown"}
                                        onMouseEnter={showDropdown} 
                                        onMouseLeave={hideDropdown}
                                    >
                                        <a href="#" className="nav-link">
                                            Giải pháp 
                                            <AiOutlineCaretDown size="16" className="ml-2" />
                                        </a>
                                        <div className="dropdown-content">
                                            <Link 
                                                className="nav-link" 
                                                activeClassName="nav-link--active" 
                                                to="/service_us"
                                            >
                                                Dịch vụ - API
                                            </Link>
                                            <Link 
                                                className="nav-link" 
                                                activeClassName="nav-link--active"  
                                                to="/products_us"
                                            >
                                                Sản phẩm
                                            </Link>
                                        </div>
                                    </div>
                                </Nav.Item>
                                <Nav.Item>
                                    <Link className="nav-link" activeClassName="nav-link--active"  to="/customers">Khách hàng</Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Link className="nav-link" activeClassName="nav-link--active"  to="/prices">Bảng giá</Link>
                                </Nav.Item>
                                <Nav.Item>
                                    <Link className="nav-link" activeClassName="nav-link--active" to="/contact_us">Liên hệ</Link>
                                </Nav.Item>
                            </Nav>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}

export default Header;
