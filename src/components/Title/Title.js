import './Title.scss';
function Title(props) {
    const titleSmall_primary = props.colorSmall;
    return (
        <div className=""
        className={ props.data_aos ? 'title-page aos-init aos-animate': 'title-page'} 
        data-aos={props.data_aos}
        >
            <span className={titleSmall_primary ? 'title-pageSmall text-success' : 'title-pageSmall text-primary'}>{props.titleSmall}</span>
            <h1 className="title-pageLagre">{props.titleLarge}</h1>
        </div>
    );
}

export default Title;
