import './Title.scss';
function TitleUnderLine(props) {
    return (
        <div className="title-page">
            <h1 className="title-underlinePage">{props.children}</h1>
        </div>
    );
}

export default TitleUnderLine;
