import { React } from 'react';
import CountUp from 'react-countup';

import './Icon_Box.scss';

function Icon_Box(props){
    return (
        <div className="icon_box">
            <div className="icon_box__img">
                <img src={props.achievement.img} />
            </div>
            <h1 className="icon_box__number text-success">
                <CountUp end={props.achievement.number} />
            </h1>
            <span className="icon_box__text">{props.achievement.text}</span>
        </div>
    );
}

export default Icon_Box;