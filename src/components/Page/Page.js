import React,{useEffect} from 'react'
import { AiOutlineArrowUp } from "react-icons/ai";
import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import Loading from '../Loading/Loading';


function Page ({ children }) {
    const [scrolled,setScrolled] = React.useState(false);
    const [loading, setLoading] = React.useState(true);
    useEffect(() => {
        setTimeout(() => setLoading(false), 2000)
    }, [])
    const handleScroll = () => {
        const offset=window.scrollY;
        if(offset > 700 ){
            setScrolled(true);
        }
        else{
            setScrolled(false);
        }   
    }
    useEffect(() => {
        window.addEventListener('scroll',handleScroll)
    })
    const scollToTop=['back_to_top'];
    if(scrolled){
        scollToTop.push('show');
    }
    function scrollTop(e){
        e.preventDefault();
        const rootElement = document.documentElement
        rootElement.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    };
    return(
        <>
            {
                loading === false ? (
                    <>
                        <Header />
                            {children}
                        <Footer />
                        <div className={ scollToTop.join(" ") }>
                            <a href="#" onClick={scrollTop}>
                                <AiOutlineArrowUp size="24" />
                            </a>
                        </div>
                    </>
                ):(
                    <>
                        <Loading/>
                    </>
                )
            }
        </>
    );
}

export default Page;