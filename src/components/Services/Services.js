import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useRouteMatch,
    useParams
} from "react-router-dom";
import './Services.scss';
import Title from '../Title/Title';

function Services(props) {
    let imgLeft = props.service.imgLeft;
    let contents = props.service.content.map( (content,index) =>
        <li key={index}>{content}</li>
    );
    if( imgLeft === true ){
        return (
            <div 
                className={ props.data_aos ? 'services-item aos-init aos-animate': 'services-item'} 
                data-aos={props.data_aos}
            >
                <div className="services-item-left">
                    <img src={props.service.img} />
                </div>
                <div className="services-item-right">
                    <Title
                        titleSmall={props.service.titleSmall}
                        titleLarge ={props.service.titleLarge}
                        colorSmall={props.service.titleSmall_color}
                    />
                    <ul>
                       {contents}
                    </ul>
                    <Link to={props.link} className="readMore-link">Tìm hiểu thêm</Link>
                </div>
            </div>
        );
    } else{
        return (
            <div 
                className={ props.data_aos ? 'services-item service-item-mobile aos-init aos-animate': 'services-item service-item-mobile' } 
                data-aos={props.data_aos}
            >
                <div className="services-item-right">
                    <Title
                        titleSmall={props.service.titleSmall}
                        titleLarge ={props.service.titleLarge}
                        colorSmall={props.service.titleSmall_color}                    
                    />
                    <ul>
                        {contents}
                    </ul>
                    <Link to={props.link} className="readMore-link">Tìm hiểu thêm</Link>
                </div>
                <div className="services-item-left">
                    <img src={props.service.img} />
                </div>
            </div>
        );
    }
    
}

export default Services;
