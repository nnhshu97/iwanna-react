import { Card } from 'react-bootstrap';
import './Customer.scss';
function Header(props) {
  return (
    <div className="customer-card">
         <Card>
            <Card.Img variant="top" src={props.customer.img_top} />
            <Card.Body>
                <Card.Text className="customer-card__text">“{props.customer.content}”</Card.Text>
                <div className="custommer-card__info">
                    <div className="custommer-card__info__name">
                        <h3>{props.customer.name}</h3>
                        <span>{props.customer.office}</span>
                    </div>
                    <div className="customer-card__logo">
                        <img src={props.customer.img_bottom} />
                    </div>
                </div> 
            </Card.Body>
        </Card>
    </div>
  );
}

export default Header;
