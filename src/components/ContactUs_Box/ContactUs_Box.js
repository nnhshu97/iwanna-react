import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    NavLink as Link
  } from "react-router-dom";
import Title from '../Title/Title';
import { Button } from 'react-bootstrap';
import ContactUs_bg from '../../assets/images/contactus_box.png';
import './ContactUs_Box.scss';

const contact_box = {
    backgroundImage: `url(${ContactUs_bg})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: '100%', 
}
const ContactUs_Box = ({ props }) => (
    <>
        <div className="ContactUs_box" style={contact_box}>
           <span>Bạn có câu hỏi? Bạn quan tâm đến dịch vụ và sản phẩm của chúng tôi?</span>
           <Title titleLarge="Chúng tôi ở đây và luôn sẵn lòng lắng nghe khách hàng" />
           <Button variant="success" className="button-success"><Link to="/contact_us" >LIÊN HỆ CHÚNG TÔI</Link></Button>
        </div> 
    </>
)

export default ContactUs_Box;