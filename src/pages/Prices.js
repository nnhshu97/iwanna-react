import React, { useState } from 'react';
import { 
    Container, 
    Row, 
    Col, 
    Button,
    Breadcrumb, 
    BreadcrumbItem
} from 'react-bootstrap';

import { AiOutlineHome, AiFillCaretRight } from "react-icons/ai";
import { FcCheckmark } from "react-icons/fc";

import {
    BrowserRouter as Router,
    Switch,
    Route,
    NavLink as Link
  } from "react-router-dom";

/* Components */
import Title from '../components/Title/Title';

/* Images */

import '../assets/scss/Prices.scss';

function Prices() {

    let title = {
        titleLarge: 'Bảng giá các gói dịch vụ'
    };
    
    /* products list text */
    let prices = [
        {   
            id: 1,
            title: 'Gói hỗ trợ',
            desc: 'Dành cho những mô hình kinh doanh nhỏ, cho người bắt đầu kinh doanh hoặc bán hàng online.',
            price: '180.000đ',
            store: '1 CỬA HÀNG/THÁNG',
            users: '3 NGƯỜI DÙNG',
            features: [
                'Không giới hạn tính năng',
                'Không phí khởi tạo',
                'Báo cáo mỗi tuần'
            ],
            button: 'LIÊN HỆ NGAY'
        },
        {
            id: 2,
            title: 'Gói chuyên nghiệp',
            desc: 'Dành cho những mô hình kinh doanh chuyên nghiệp',
            price: '350.000đ',
            store: '1 CỬA HÀNG/THÁNG',
            users: 'KHÔNG GIỚI HẠN NGƯỜI DÙNG',
            features: [
                'Không giới hạn tính năng',
                'Không phí khởi tạo',
                'Báo cáo mỗi tuần'
            ],
            button: 'LIÊN HỆ NGAY'
        }
    ];

    let lists = [
        [
            'Lập hóa đơn bán hàng',
            'Quản lý đặt hàng, đổi trả hàng',
            'Quản lý nhập hàng, chuyển hàng',
            'Quản lý hàng hóa',
            'Quản lý tồn kho',
            'Quản lý khách hàng',
            'Quản lý nhân viên',
        ],
        [
            'Quản lý nhà cung cấp',
            'Quản lý sổ quỹ (Thu/chi)',
            'Quản lý công nợ',
            'Quản lý chính sách giá linh hoạt',
            'Quản lý chương trình khuyến mãi, voucher',
            'Quản lý và bán hàng trên thiết bi di động',
            'Báo cáo kinh doanh thông minh và chi tiết'
        ],
        [
            'Hệ thống cảnh báo tình hình kinh doanh',
            'Hệ thống chăm sóc khách hàng',
            'Hệ thống tích điểm',
            'Quản lý Lô - Hạn sử dụng hàng hóa',
            'Quản lý sản xuất, định lượng nguyên vật liệu',
            'Quản lý SERIAL/ IMEI cho ngành điện tử',
            'Quản lý order/ phòng bàn/giờ'
        ]
    ];

    return (
        <div className="wrapper prices_wrapper">
            <div className="prices_breadcrumb">
                <Container>
                    <Row>
                        <Col md={12}>
                            <Breadcrumb>
                                <Breadcrumb.Item href="/"><AiOutlineHome size="24" /></Breadcrumb.Item>
                                <Breadcrumb.Item active>
                                    Bảng giá
                                </Breadcrumb.Item>
                            </Breadcrumb>
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className="prices-features">
                <Container>
                    <Row>
                        <Col md={12} className="text-center">
                        <Title titleLarge={title.titleLarge}></Title>
                        <span>Hãy lựa chọn giải pháp phù hợp với nhu cầu của bạn và dùng thử miễn phí 7 ngày</span>
                        </Col>
                        {
                            prices.map( (price) =>
                                <Col md={6} key={price.id}>
                                    <div className="prices_package">
                                        <h3 className="prices_package__title">
                                            {price.title}
                                        </h3>
                                        <span className="prices_package__subtTitle">{price.desc}</span>
                                        <h1 className="text-primary prices_package__price">{price.price}</h1>
                                        <span className="text-uppercase font-weight-bold d-block">{price.store}</span>
                                        <span className="text-primary font-weight-bold d-block">{price.users}</span>
                                        <ul>
                                            {
                                                price.features.map( (item,index) =>
                                                    <li key={index}><FcCheckmark size="14" className="mr-2" />{item}</li>
                                                )
                                            }
                                        </ul>
                                        <Button color="success" className="button-success">
                                            <Link to="/contact_us">{price.button}</Link>
                                        </Button>
                                    </div>
                                </Col>
                            )
                        }
                    </Row>
                </Container>
            </div>
            <div className="prices-list">
                <Container>
                    <Row>
                        <Col md={12} className="text-center">
                            <Title titleLarge="Tất cả các gói đều không giới hạn tính năng"></Title>
                            <div className="prices-list__ul">
                                {
                                    lists.map( (item,index) => 
                                        <ul key={index}>
                                            {
                                                item.map( (item,index) =>
                                                    <li key={index}><AiFillCaretRight size="14" color="#9ACF62" className="mr-2" />{item}</li>
                                                )
                                            }
                                        </ul>
                                    )
                                }
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        </div>
    );
}

export default Prices;
