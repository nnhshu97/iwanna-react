import { Container, Row, Col, Breadcrumb, Button, BreadcrumbItem, Form } from 'react-bootstrap';
import { AiOutlineHome } from "react-icons/ai";
/* Components */
import Title from '../components/Title/Title';
import SvgIcon from '../components/Svg_Icon/Svg_Icon';
import Icon_Box from '../components/Icon_Box/Icon_Box';


/* Images */
import bg_AboutUs from '../assets/images/bg-aboutus.png';

/* tabs icon */
import icon_1 from '../assets/images/svg-icons/icon_1.svg';
import icon_2 from '../assets/images/svg-icons/icon_2.svg';
import icon_3 from '../assets/images/svg-icons/icon_3.svg';

import '../assets/scss/Customers.scss';


function Contact_Us() {

    return (
        <div className="wrapper">
            <div className="contactUs_breadcrumb">
                <Container>
                    <Row>
                        <Col md={12}>
                            <Breadcrumb>
                                <Breadcrumb.Item href="/"><AiOutlineHome size="24" /></Breadcrumb.Item>
                                <Breadcrumb.Item active>
                                    Liên hệ
                                </Breadcrumb.Item>
                            </Breadcrumb>
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className="contactUs_intro pb-5">
                <Container>
                    <Row>
                        <Col md={12} className="pb-4">
                            <div className="customers_intro__content text-left">
                                <Title titleLarge="Liên hệ chúng tôi"></Title>
                            </div>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={5}>
                            <SvgIcon img={icon_1} size="24px">
                                <span>
                                    <b>Trụ sở chính:</b>
                                    <br/>
                                    Số 5, Ngõ 128 Phố Vọng, Phương Liệt, Thanh Xuân, Hà Nội, Việt Nam.
                                </span>
                                <span>
                                    <b>Thứ Hai – Thứ Bảy:</b>
                                    <br/>
                                    10:00 – 18:00 GMT+6
                                </span>
                            </SvgIcon>
                            <span className="d-block font-weight-bold mt-4 mb-4">Bộ phận Sales</span>
                            <SvgIcon img={icon_3} size="24px">
                                <span>
                                    84 (0)37 701 2386 - Zalo - (10:00 - 18:00 GMT+6)
                                </span>
                            </SvgIcon>
                            <SvgIcon img={icon_2} size="24px">
                                <span>
                                    contact@iwannatech.com
                                </span>
                            </SvgIcon>
                            <span className="d-block font-weight-bold mt-4 mb-4">Hỗ trợ kỹ thuật</span>
                            <SvgIcon img={icon_3} size="24px">
                                <span>
                                    +84 (0)98 934 2400 - (10:00 - 18:00 GMT+6)
                                </span>
                            </SvgIcon>
                            <SvgIcon img={icon_2} size="24px">
                                <span>
                                    sonhs@iwannatech.com
                                </span>
                            </SvgIcon>
                        </Col>
                        <Col md={7}>
                            <Form>
                                <span className="d-block mb-3">Nếu quý khách có thắc mắc hay ý kiến phản hồi đóng góp xin vui lòng điền vào Form dưới đây và gửi cho chúng tôi. Xin chân thành cảm ơn!</span>
                                <Row>
                                    <Col md={6}>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Họ và tên *</Form.Label>
                                            <Form.Control type="text" placeholder="Phạm Minh" />
                                        </Form.Group>
                                    </Col>
                                    <Col md={6}>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Email *</Form.Label>
                                            <Form.Control type="email" placeholder="minhtuong@gmail.com" />
                                        </Form.Group>
                                    </Col>
                                    <Col md={6}>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Tên công ty của bạn *</Form.Label>
                                            <Form.Control type="text" placeholder="InVision Studio" />
                                        </Form.Group>
                                    </Col>
                                    <Col md={6}>
                                        <Form.Group controlId="formBasicEmail">
                                            <Form.Label>Số điện thoại *</Form.Label>
                                            <Form.Control type="text" placeholder="0902412345" />
                                        </Form.Group>
                                    </Col>
                                    <Col md={12}>
                                        <Form.Group controlId="formGridState">
                                            <Form.Label>Gửi tới phòng/ban:</Form.Label>
                                            <Form.Control as="select" defaultValue="Choose...">
                                                <option>Phòng hỗ trợ kỹ thuật</option>
                                                <option>Phòng hỗ trợ kỹ thuật</option>
                                            </Form.Control>
                                        </Form.Group>
                                    </Col>
                                    <Col md={12}>
                                        <Form.Group controlId="">
                                            <Form.Label>Gửi tới phòng/ban:</Form.Label>
                                            <Form.Control as="textarea" rows={3} />
                                        </Form.Group>
                                    </Col>
                                    <Col md={12} className="text-right">
                                        <Button variant="success" className="button-success" type="submit">
                                            Gửi thông tin
                                        </Button>
                                    </Col>
                                </Row>
                                
                            </Form>
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className="contactUs_map" style={{height: 320 + 'px'}}>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.8995332599234!2d105.84055331502915!3d20.996663994248852!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ac70276e2d45%3A0x6267a24044035d9d!2zMTI4IFBo4buRIFbhu41uZywgUGjGsMahbmcgTGnhu4d0LCBIYWkgQsOgIFRyxrBuZywgSMOgIE7hu5lpLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1608705224479!5m2!1svi!2s" width="100%" height="320" frameBorder="0" allowFullScreen="" aria-hidden="false" tabIndex="0"></iframe>
            </div>
        </div>
    );
}

export default Contact_Us;
