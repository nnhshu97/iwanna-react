import { Container, Row, Col, Button, Card, Image, Breadcrumb, BreadcrumbItem } from 'react-bootstrap';
import { AiOutlineHome } from "react-icons/ai";
import { FcCheckmark } from "react-icons/fc";


/* Components */
import Title from '../../components/Title/Title';
import ContactUs_Box from '../../components/ContactUs_Box/ContactUs_Box';
import SvgIcon from '../../components/Svg_Icon/Svg_Icon';

/* Images */

import feature_1 from '../../assets/images/svg-icons/feature_1.svg';
import feature_2 from '../../assets/images/svg-icons/feature_2.svg';
import feature_3 from '../../assets/images/svg-icons/feature_3.svg';
import feature_4 from '../../assets/images/svg-icons/feature_4.svg';
import ServiceUs_bg from '../../assets/images/serviceUs_logo.png';
import Digitalized_1 from '../../assets/images/img_12.png';
import advantages_img from '../../assets/images/advantages_1.png';

/* scss */
import './Digitalized_Text.scss';

function Digitalized_Text() {

    let title_aboutUs= {
        titleSmall: 'DỊCH VỤ',
        titleSmall_color : true,
        titleLarge: 'Số Hóa Văn Bản'
    };

    let title = {
        titleLarge: 'Các tính năng chính của phần mềm'
    };
    let title_advantage = {
        titleLarge: 'Ưu điểm và ứng dụng của phần mềm'
    };
    let advantages_list = [
        'Rút ngắn thời gian tác nghiệp',
        'Tăng tính bảo mật cho tài liệu',
        'Đảm bảo tài liệu được lưu trữ an toàn, lâu dài'
    ];

    let style = {
        display: 'grid',
        gridTemplateColumns: '50% 50%'
    };

    return (
        <div className="wrapper">
            <div className="DetailService_breadcrumb">
                <Container>
                    <Row>
                        <Col md={12}>
                            <Breadcrumb>
                                <Breadcrumb.Item href="/"><AiOutlineHome size="24" /></Breadcrumb.Item>
                                <Breadcrumb.Item active>
                                    Dịch vụ - API
                                </Breadcrumb.Item>
                            </Breadcrumb>
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className="DetailService">
                <Container>
                    <Row>
                        <Col md={2}></Col>
                        <Col md={8} className="text-center">
                            <Title 
                                titleSmall={title_aboutUs.titleSmall} 
                                titleLarge={title_aboutUs.titleLarge} 
                                colorSmall={title_aboutUs.titleSmall_color}
                                data_aos="zoom-in"
                            />
                            <span>
                                Số hóa tài liệu là hình thức đang phổ biến khắp thế giới, là xu thế toàn cầu, 
                                từ tài liệu trên giấy tờ giờ được chuyển sang dạng file để lưu trữ trên máy tính, 
                                hay còn gọi là số hóa tài liệu lưu trữ.
                            </span>
                            <img src={Digitalized_1} className="mt-5" data-aos="zoom-in" />
                        </Col>
                        <Col md={2}></Col>
                        <Col md={12}>
                            <div className="DetailService_context" style={style} >
                                <div className="text-left" data-aos="fade-up">
                                    <span className="text-success aboutUs_intro__subtitle">TÍNH NĂNG</span>
                                    <Title titleLarge={title.titleLarge}></Title>
                                </div>
                                <div style={{ margin: '60px'}} data-aos="fade-up">
                                    <span>
                                        Số hoá văn bản ra đời như một giải pháp thông minh tối ưu giúp các doanh 
                                        nghiệp giảm thiểu chi phí và thời gian in ấn, quản lý đồng thời nâng cao 
                                        hiệu quả sử dụng các tài liệu này.
                                    </span>
                                </div>
                            </div>
                            <div className="DetailService_feature" style={style}>
                                <SvgIcon img={feature_2} size="92px" data_aos="fade-up">
                                    <span>
                                        <b style={{fontSize: "24px"}}>Nhận dạng</b>
                                        <br/>
                                        Nhận dạng văn bản tiếng Việt thông minh: OCR
                                    </span>
                                </SvgIcon>
                                <SvgIcon img={feature_4} size="92px" data_aos="fade-up">
                                    <span>
                                        <b style={{fontSize: "24px"}}>Tìm kiếm</b>
                                        <br/>
                                        Tìm kiếm chi tiết nhanh chóng, rút ngắn thời gian tác nghiệp trong các khâu
                                    </span>
                                </SvgIcon>
                                <SvgIcon img={feature_3} size="92px" data_aos="fade-up">
                                    <span>
                                        <b style={{fontSize: "24px"}}>Tự động hoá</b>
                                        <br/>
                                        Lập chỉ mục tự động thông minh, cho phép quét và quản trị tài liệu
                                    </span>
                                </SvgIcon>
                                <SvgIcon img={feature_1} size="92px" data_aos="fade-up">
                                    <span>
                                        <b style={{fontSize: "24px"}}>Quản trị người dùng</b>
                                        <br/>
                                        Nâng cao hiệu quả kinh doanh, tiết kiệm được chi phí quản lý lưu trữ về lâu dài.
                                    </span>
                                </SvgIcon>
                            </div>
                            <div className="is-line"></div>
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className="DetailService_context__advantages">
                <Container>
                    <Row>
                        <Col md={6} data-aos="zoom-in">
                            <img src={advantages_img} />
                        </Col>
                        <Col md={6} data-aos="zoom-in">
                            <span className="text-success aboutUs_intro__subtitle">ƯU ĐIỂM VÀ ỨNG DỤNG</span>
                            <Title titleLarge={title_advantage.titleLarge}></Title>
                            <span>
                                Số hoá văn bản ra đời như một giải pháp thông minh tối ưu giúp các 
                                doanh nghiệp giảm thiểu chi phí và thời gian in ấn, quản lý đồng thời 
                                nâng cao hiệu quả sử dụng các tài liệu này.
                            </span>
                            <ul className="mt-3">
                                {
                                    advantages_list.map( (item) => 
                                        <li key={ item } className="d-flex align-items-center">
                                            <FcCheckmark size="24" />
                                            <span className="font-weight-bold ml-2">{ item }</span>
                                        </li>
                                    )
                                }
                            </ul>
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className="DetailService_brands">
                <Container>
                    <Row>
                        <Col md={12} className="text-center">
                            <span>Được hàng trăm công ty và doanh nghiệp sử dụng trện toàn đất nước</span>
                            <img src={ServiceUs_bg} className="mt-5 mb-5" data-aos="fade-up" />
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className="ServiceUS_contact">
                <Container>
                    <Row>
                        <Col md={12} data-aos="fade-up">
                            <ContactUs_Box/>
                        </Col>
                    </Row>
                </Container>
            </div>
        </div>
    );
}

export default Digitalized_Text;
