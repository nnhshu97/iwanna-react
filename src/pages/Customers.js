import { Container, Row, Col, Image, Breadcrumb, BreadcrumbItem, Tabs, Tab } from 'react-bootstrap';
import { AiOutlineHome } from "react-icons/ai";
/* Components */
import Title from '../components/Title/Title';
import Icon_Box from '../components/Icon_Box/Icon_Box';
import TitleUnderLine from '../components/Title/TitleUnderLine';
import Services from '../components/Services/Services';
import Customer from '../components/Customer/Customer';
import Card_Post from '../components/Blog_Card/Card';
import Page from '../components/Page/Page';
import ContactUs_Box from '../components/ContactUs_Box/ContactUs_Box';

/* Images */
import bg_AboutUs from '../assets/images/bg-aboutus.png';
import AboutUs_1 from '../assets/images/aboutUs_1.png';
import puzzle from '../assets/images/svg-icons/puzzle.svg';
import hand_shake from '../assets/images/svg-icons/hand-shake.svg';
import user from '../assets/images/svg-icons/user.svg';
import processing from '../assets/images/about_us/processing.png';
import price from '../assets/images/about_us/price.png';
import cloud from '../assets/images/about_us/cloud.png';
import vision from '../assets/images/tamnhin.png';
import customters_bg from '../assets/images/customers_bg.png';
import customser_logo from '../assets/images/customers_logo.png';
/* tabs icon */
import quotes from '../assets/images/svg-icons/quotes.svg';
import tab_1 from '../assets/images/svg-icons/tab_1.svg';
import tab_2 from '../assets/images/svg-icons/tab_2.svg';
import tab_3 from '../assets/images/svg-icons/tab_3.svg';

/* Reacr Swiper */
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper.scss';
import 'swiper/components/pagination/pagination.scss';

import '../assets/scss/Customers.scss';



/* Use core React Swiper */
SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);

function Customers() {
    let title = {
        titleLarge: 'Các khách hàng'
    };

    let style_banner = { 
        backgroundImage: `url(${bg_AboutUs})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: '100%',
    };

    /* services list text */
    let achievement_list = [
        {
            id: 1,
            img: `${puzzle}`,
            number: '+10',
            text: 'năm kinh nghiệm'
        },
        {
            id: 2,
            img: `${user}`,
            number: '2M',
            text: 'khách hàng'
        },
        {
            id: 3,
            img: `${hand_shake}`,
            number: '+70',
            text: 'doanh nghiệp'
        }
    ];

    /* services list text */
    let chooseUs_list = [
        {
            id: 1,
            img: `${processing}`,
            title: 'Trải nghiệm tốt',
            text: 'Cá nhân hóa trải nghiệm khách hàng khi mua sắm và sử dụng'
        },
        {
            id: 2,
            img: `${cloud}`,
            title: 'Tự động hoá',
            text: 'Tự động hóa nhận diện và quản lý lượt ra vào cơ quan, trường học'
        },
        {
            id: 3,
            img: `${price}`,
            title: 'Tiết kiệm chi phí',
            text: 'Tiết giảm chi phí hoạt động cho doanh nghiệp, tổ chức'
        }
    ];

    return (
        <div className="wrapper">
            <div className="customers_breadcrumb">
                <Container>
                    <Row>
                        <Col md={12}>
                            <Breadcrumb>
                                <Breadcrumb.Item href="/"><AiOutlineHome size="24" /></Breadcrumb.Item>
                                <Breadcrumb.Item active>
                                    Khách hàng
                                </Breadcrumb.Item>
                            </Breadcrumb>
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className="customers_intro">
                <Container>
                    <Row>
                        <Col md={12}>
                            <div className="customers_intro__content">
                                <img src={customters_bg} />
                                <div className="customers_intro__content__text">
                                    <p>
                                        Công ty TNHH Iwanna Tech luôn sẵn sàng mang tới những trải nghiệm tốt nhất cho 
                                        Khách hàng về dịch vụ trực tuyến để cùng Khách hàng phát triển trong thời đại CNTT như hiện nay.
                                    </p>
                                    <img src={quotes} />
                                </div>
                                <Title titleLarge="Các khách hàng"></Title>
                                <div className="customers_intro__content__logo">
                                    <img src={customser_logo} />
                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className="customers_contact">
                <Container>
                    <Row>
                        <Col md={12}>
                            <ContactUs_Box/>
                        </Col>
                    </Row>
                </Container>

            </div>
        </div>
    );
}

export default Customers;
