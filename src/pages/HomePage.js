import { Container, Row, Col, Button, Card, Image } from 'react-bootstrap';

/* Components */
import Title from '../components/Title/Title';
import TitleUnderLine from '../components/Title/TitleUnderLine';
import Services from '../components/Services/Services';
import Customer from '../components/Customer/Customer';
import Card_Post from '../components/Blog_Card/Card';

/* Images */
import Background from '../assets/images/bg-homepage.png';
import ImageBanner from '../assets/images/image-1.png';
import Logo from '../assets/images/logo.png';
import AboutImage from '../assets/images/about-us.png';
import Service_1 from '../assets/images/services-1.png';
import Service_2 from '../assets/images/services-2.png';
import Service_3 from '../assets/images/services-3.png';
import Product_1 from '../assets/images/sp-1.png';
import Product_2 from '../assets/images/sp-2.png';
import Bg_kh from '../assets/images/bg-kh.png';
import Customer_1 from '../assets/images/img-customer-1.png';
import LogoCustomer from '../assets/images/logo-customer-1.png';
import Brands from '../assets/images/brand_list.png';
import Img_Post from '../assets/images/img_post_1.png';
import Contact_Img from '../assets/images/img-contact.png';
import Zalo_Icon from '../assets/images/icon-zalo.png';
import Bg_Contact from '../assets/images/bg-contact.png';

/* Reacr Swiper */
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper.scss';
import 'swiper/components/pagination/pagination.scss';

/* Use core React Swiper */
SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);

function HomePage() {
    let title = {
        titleSmall: 'CHÚNG TÔI',
        titleLarge: 'Cung cấp các giải pháp toàn diện'
    };

    let title_aboutUs= {
        titleSmall: 'GIỚI THIỆU',
        titleSmall_color : true,
        titleLarge: 'Chúng tôi là ai?'
    };

    let style_banner = { 
        backgroundImage: `url(${Background})`,
        height: '100vh',
        backgroundRepeart: 'no-repeart'
    };
    let style_customer = {
        backgroundImage: `url(${Bg_kh})`,
        backgroundRepeart: 'no-repeart'
    }
    let style_contact = {
        backgroundImage: `url(${Bg_Contact})`,
        backgroundRepeart: 'no-repeart'
    }
    /* services list text */
    let services_list = [
        {
            id: 1,
            img: `${Service_3}`,
            imgLeft: true,
            titleSmall: 'DỊCH VỤ',
            titleSmall_color: true,
            titleLarge: 'Số Hóa Văn Bản',
            content: [
                'Số hóa hóa đơn, văn bản',
                'Nhận dạng CMND, Passport, Giấy phép lái xe'
            ],
            link: '/service_us/digitalized_text'
        },
        {
            id: 2,
            imgLeft: false,
            img: `${Service_1}`,
            titleSmall: 'DỊCH VỤ',
            titleSmall_color: true,
            titleLarge: 'Giao thông thông minh',
            content: [
                'Nhận diện biển số',
                'Phân loại phương tiện',
                'Đếm lưu lượng phương tiện',
                'Xác định vi phạm: Sai làn, ngược chiều, dừng đỗ, rớt đồ vật trên đường'
            ],
            link: '/service_us/digitalized_text'
        },
        {
            id: 3,
            imgLeft: true,
            img: `${Service_2}`,
            titleSmall: 'DỊCH VỤ',
            titleSmall_color: true,
            titleLarge: 'Phân tích Camera – Face ID',
            content: [
                'Quản lý ra vào bằng khuôn mặt',
                'Điểm danh bằng khuôn mặt',
                'Giám sát khu vực an ninh',
                'Đếm số người'
            ],
            link: '/service_us/digitalized_text'
        }
    ];
    /* products list text */
    let products = [
        {
            img: `${Product_1}`,
            title: 'Phần mềm bãi đỗ xe thông minh',
            link: '#'
        },
        {
            img: `${Product_2}`,
            title: 'Phần mềm quản lý trường học và trung tâm giáo dục',
            link: '#'
        }
    ];

    let customer_list = [
        {
            id: 1,
            img_top: `${Customer_1}`,
            img_bottom: `${LogoCustomer}`,
            name:'Ivy Nguyen',
            office: 'Senior Product Manager',
            content:'Phần mềm khiến chúng tôi bớt vất vả hơn trong quá trình hoạt động của công ty, tối ưu hoá quy trình rất nhiều. Sản phẩm thật sự vượt xa mong đợi'
        },
        {
            id: 2,
            img_top: `${Customer_1}`,
            img_bottom: `${LogoCustomer}`,
            name:'Ivy Nguyen',
            office: 'Senior Product Manager',
            content:'Phần mềm khiến chúng tôi bớt vất vả hơn trong quá trình hoạt động của công ty, tối ưu hoá quy trình rất nhiều. Sản phẩm thật sự vượt xa mong đợi'
        }
    ];

    let posts = [
        {
            id: 1,
            img: `${Img_Post}`,
            title: 'Tại sao cần sử dụng phần mềm tính lương?',
            content: 'Đối với vấn đề tính lương trong nhân sự là một vấn đề nhạy cảm, cần độ chính xác cao cũng như yêu cầu',
            author: 'iwannatech.com — DEC 26, 2020'
        },
        {
            id: 2,
            img: `${Img_Post}`,
            title: 'Thúc đẩy ứng dụng trí tuệ nhân tạo trong giải mã gene',
            content: 'Việc ứng dụng di truyền học trong cải thiện việc chăm sóc sức khỏe bệnh nhân, giải mã gene…',
            author: 'iwannatech.com — DEC 26, 2020'
        },
        {
            id: 3,
            img: `${Img_Post}`,
            title: 'Phần mềm nhân sự có thực sự cần thiết hay không ?',
            content: 'Phần lớn các tổ chức hiện nay đều nhận thấy tầm quan trọng của chức năng nhân sự trong hoạt động quản trị.',
            author: 'iwannatech.com — DEC 26, 2020'
        }
    ]

    return (
        <div className="wrapper">
            <div className="home-banner" style={style_banner}>
                <Swiper
                    spaceBetween={50}
                    slidesPerView={1}
                    pagination={{ clickable: true }}
                >
                    <SwiperSlide>
                        <div className="home-banner-item">
                            <Container>
                                <Row>
                                    <Col md={4} xs={12} data-aos="zoom-in">
                                        <Title titleSmall={title.titleSmall} titleLarge={title.titleLarge}></Title>
                                        <span className="d-block mb-5">Giúp cho doanh nghiệp giải quyết được những khó khăn trong công tác quản lý</span>
                                        <Button variant="success" className="button-success">Thử ngay</Button>
                                    </Col>
                                    <div className="home-bannerImage">
                                        <img src={ImageBanner} width="100%"  />
                                    </div>
                                </Row>
                            </Container>
                        </div>
                    </SwiperSlide>
                    <SwiperSlide>
                        <div className="home-banner-item">
                            <Container>
                                <Row>
                                    <Col md={4} xs={12} data-aos="zoom-in">
                                        <Title titleSmall={title.titleSmall} titleLarge={title.titleLarge} />
                                        <span className="d-block mb-5">Giúp cho doanh nghiệp giải quyết được những khó khăn trong công tác quản lý</span>
                                        <Button variant="success" className="button-success">Thử ngay</Button>
                                    </Col>
                                    <div className="home-bannerImage">
                                        <img src={ImageBanner} width="100%" />
                                    </div>
                                </Row>
                            </Container>
                        </div>
                    </SwiperSlide>
                </Swiper>
            </div>
            <div className="home-aboutUs">
                <Container>
                    <Row>
                        <Col md={12} className="text-center mb-4">
                            <Title 
                                titleSmall={title_aboutUs.titleSmall} 
                                titleLarge={title_aboutUs.titleLarge} 
                                colorSmall={title_aboutUs.titleSmall_color}
                            />
                        </Col>
                        <Col md={6} data-aos="fade-up">
                            <p className="text-justify">
                                Với kỷ nguyên công nghệ số kết nối toàn cầu và thời đại AI. Cùng sự phát triển mạnh mẽ của Internet, Công nghệ thông tin, Điện tử viễn thông, Tự động hóa vv…. Công ty IwannaTech đã ra đời với sứ mệnh cung cấp các giải pháp công nghệ số và ngành công nghiệp thông minh. 
                            </p>
                            <p className="text-justify">Chúng tôi quan niệm rằng, việc kinh doanh phải đi đôi với đóng góp và chia sẻ theo phương châm thiết thực: <b>Thận trọng, chú tâm và quan sát.</b></p>
                            <img className="home-aboutUs-imageLeft" src={Logo}/>
                        </Col>
                        <Col md={6} data-aos="fade-up">
                            <img src={AboutImage} className="home-aboutUs-imageRight" />
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className="home-servicesUs">
                <Container>
                    <Row>
                        <Col md={12} className="text-center">
                            <TitleUnderLine>Dịch vụ - API</TitleUnderLine>
                        </Col>
                        <Col md={12}>
                            {
                                services_list.map( (service) => 
                                    <div key={service.id}>
                                        <Services
                                            service={service}
                                            link={service.link}
                                            data_aos="fade-up"
                                        />
                                    </div>
                                )
                            }
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className="home-productsUs">
                <Container>
                    <Row>
                        <Col md={12} className="text-center mb-4" data-aos="fade-up">
                            <TitleUnderLine>Sản phẩm</TitleUnderLine>
                        </Col>
                        {
                            products.map( (product,index) => 
                                <Col md={6} key={index}>
                                    <div className="home-productUs-item" data-aos="fade-up"
                                    data-aos-delay="500"
                                    data-aos-easing="ease-in-out-cubic" >
                                        <Card>
                                            <Card.Img variant="top" src={product.img} />
                                            <Card.Body>
                                                <Card.Text className="product-card__subTitle">SẢN PHẨM</Card.Text>
                                                <Card.Title className="product-card__title">{product.title}</Card.Title>
                                                <a href={product.link} className="readMore-link">Tìm hiểu thêm</a>    
                                            </Card.Body>
                                        </Card>
                                    </div>
                                </Col>
                            )
                        }
                    </Row>
                </Container>
            </div>
            <div className="home-customer" style={style_customer}>
                <Container>
                    <Row>
                        <Col 
                            md={4} 
                            data-aos="fade-up" 
                            data-aos-delay="500"
                            data-aos-easing="ease-in-out-cubic" 
                        >
                            <Swiper
                                spaceBetween={50}
                                slidesPerView={1}
                                navigation
                                pagination={{ clickable: true }}
                                scrollbar={{ draggable: true }}
                            >
                                {
                                    customer_list.map( (customer) =>
                                    <SwiperSlide key={customer.id}>
                                        <Customer customer={customer}></Customer>
                                    </SwiperSlide>
                                    )
                                }
                            </Swiper>
                        </Col>
                        <Col md={1}></Col>
                        <Col md={7} data-aos="fade-up">
                            <div className="home-customer__title">
                                <h1 className="text-white">Được tin tưởng và sử dụng bởi <br/> <span className="title_color">+100</span> doanh nghiệp và cá nhân trong & ngoài nước</h1>
                            </div>
                            <span className="home-customer__brands">Các khách hàng của chúng tôi</span>
                            <img src={Brands} className="home-customer__brandsImage"></img>
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className="home-posts">
                <Container>
                    <Row>
                        <Col 
                            md={12} 
                            className="text-center mb-5"
                            data-aos="fade-up" 
                            data-aos-delay="500"
                            data-aos-easing="ease-in-out-cubic" 
                        >
                            <Title titleLarge="Tin tức" />
                        </Col>
                        <Col md={12}>
                            <div 
                                className="home-posts__container" 
                                data-aos="fade-up" 
                                data-aos-delay="500"
                                data-aos-easing="ease-in-out-cubic" 
                            >
                                {
                                    posts.map( (post) =>
                                        <Card_Post post={post}  key={post.id} />
                                    )
                                }
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className="home-contact eqeqe" style={style_contact}>
                <Container>
                    <Row>
                        <Col md={12} className="text-center">
                            <Image 
                                src={Contact_Img} 
                                className="mb-5 home-contact__img" 
                                data-aos="zoom-in" 
                                data-aos-delay="500"
                                data-aos-easing="ease-in-out-cubic"  
                            />
                            <Title titleLarge="Hỗ trợ & Giúp đỡ" />
                            <div className="home-contact__content" data-aos="fade-up">
                                Vui lòng liên hệ với chúng tôi để được nhận thông tin về sản phẩm và dịch vụ.
                                <br/> Tất cả vì niềm tin khách hàng!
                            </div>
                            <div className="home-contact__info" data-aos="fade-up">
                                <div className="home-contact__info__text">
                                    <h3 className="text-success">+84 (0)79 379 8987</h3>
                                    <span>THỜI GIAN:: 10:00–18:00 GMT+6</span>
                                </div>
                                <div className="home-contact__info__img"> 
                                    <Image src={Zalo_Icon} />
                                </div>
                            </div>
                            <span className="text-success mt-3 d-block" data-aos="fade-up">Sonhs@iwannatech.com</span>
                        </Col>
                    </Row>
                </Container>
            </div>
        </div>
    );
}

export default HomePage;
