import { Container, Row, Col, Image, Breadcrumb, BreadcrumbItem, Tabs, Tab } from 'react-bootstrap';
import { AiOutlineHome } from "react-icons/ai";

/* Components */
import Title from '../components/Title/Title';
import Icon_Box from '../components/Icon_Box/Icon_Box';
import TitleUnderLine from '../components/Title/TitleUnderLine';
import Services from '../components/Services/Services';
import Customer from '../components/Customer/Customer';
import Card_Post from '../components/Blog_Card/Card';
import Page from '../components/Page/Page';

/* Images */
import bg_AboutUs from '../assets/images/bg-aboutus.png';
import AboutUs_1 from '../assets/images/aboutUs_1.png';
import puzzle from '../assets/images/svg-icons/puzzle.svg';
import hand_shake from '../assets/images/svg-icons/hand-shake.svg';
import user from '../assets/images/svg-icons/user.svg';
import processing from '../assets/images/about_us/processing.png';
import price from '../assets/images/about_us/price.png';
import cloud from '../assets/images/about_us/cloud.png';
import vision from '../assets/images/tamnhin.png';
/* tabs icon */
import tab_1 from '../assets/images/svg-icons/tab_1.svg';
import tab_2 from '../assets/images/svg-icons/tab_2.svg';
import tab_3 from '../assets/images/svg-icons/tab_3.svg';

/* Reacr Swiper */
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper.scss';
import 'swiper/components/pagination/pagination.scss';

import '../assets/scss/AboutUs.scss';


/* Use core React Swiper */
SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);

function About_Us() {
    let title = {
        titleLarge: 'Chúng tôi là ai?'
    };

    let style_banner = { 
        backgroundImage: `url(${bg_AboutUs})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: '100%',
    };

    /* services list text */
    let achievement_list = [
        {
            id: 1,
            img: `${puzzle}`,
            number: 10,
            text: 'năm kinh nghiệm'
        },
        {
            id: 2,
            img: `${user}`,
            number: 2000,
            text: 'khách hàng'
        },
        {
            id: 3,
            img: `${hand_shake}`,
            number: 70,
            text: 'doanh nghiệp'
        }
    ];

    /* services list text */
    let chooseUs_list = [
        {
            id: 1,
            img: `${processing}`,
            title: 'Trải nghiệm tốt',
            text: 'Cá nhân hóa trải nghiệm khách hàng khi mua sắm và sử dụng'
        },
        {
            id: 2,
            img: `${cloud}`,
            title: 'Tự động hoá',
            text: 'Tự động hóa nhận diện và quản lý lượt ra vào cơ quan, trường học'
        },
        {
            id: 3,
            img: `${price}`,
            title: 'Tiết kiệm chi phí',
            text: 'Tiết giảm chi phí hoạt động cho doanh nghiệp, tổ chức'
        }
    ];

    return (
        <div className="wrapper">
            <div className="aboutUs_breadcrumb">
                <Container>
                    <Row>
                        <Col md={12}>
                            <Breadcrumb>
                                <Breadcrumb.Item href="/"><AiOutlineHome size="24" /></Breadcrumb.Item>
                                <Breadcrumb.Item active>
                                    Về chúng tôi
                                </Breadcrumb.Item>
                            </Breadcrumb>
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className="aboutUs_intro" style={style_banner}>
                <Container>
                    <Row>
                        <Col md={2}></Col>
                        <Col md={8}>
                            <Title titleLarge={title.titleLarge}></Title>
                            <span className="text-success aboutUs_intro__subtitle">GIỚI THIỆU CHUNG</span>
                            <p>Được thành lập từ năm 2007, IwannaTech luôn tập trung vào nghiên cứu và chuyển giao các giải pháp công nghệ tiên tiến trên cơ sở hiểu được nhu cầu của khách hàng để cung cấp cho khách hàng các giải pháp CNTT thông minh và phù hợp. với thực tế, để mang lại giá trị và hiệu quả cao cho khách hàng.</p>
                            <p>Công Ty IwannaTech được sự chấp thuận của chủ tịch TP Hà Nội và Sở kế hoạch đầu tư chính thức cấp giấy phép hoạt động từ ngày 30.08.2012. Hoạt động chủ yếu trong lĩnh vực sản xuất, thương mại, tư vấn đào tạo về phần mềm, dịch vụ phần mềm internet và các dịch vụ công nghệ thông tin nói chung.</p>
                            <p>Với phương châm đặt lợi ích của khách hàng lên trên hết, giúp cho quý khách hàng nâng cao chất lượng Quản lý, hiệu quả trong kinh doanh, tối ưu các giá trị cho tổ chức, Doanh nghiệp. Chúng tôi không ngừng nghiên cứu, phát triển và cung cấp các sản phẩm Phần mềm, các giải pháp trên nền tảng CNTT nói chung. Bên cạnh đó không ngừng thu hút nhân tài, nâng cấp liên tục trình độ cán bộ công nhân viên với tiêu chí đủ cả Tâm, Tầm và Tài để cung cấp và đáp ứng nhu cầu ngày càng cao của thị trường.
                            </p>
                            <p>Với khẩu hiệu “Kiến tạo niềm tin – Chia sẻ thành công” IwannaTech luôn nỗ lực mang lại cho quý khách hàng sự hài lòng, thời gian và giá trị khi sử dụng các sản phẩm, dịch vụ từ IwannaTech.</p>
                            <Image src={AboutUs_1} />
                        </Col>
                        <Col md={2}></Col>
                    </Row>
                </Container>
            </div>
            <div className="aboutUs_achievement">
                <Container>
                    <Row>
                        <Col md={12}>
                            <div className="aboutUs_achievement__list">
                                {
                                    achievement_list.map( (item,index) =>
                                        <Icon_Box achievement={item} key={index} />
                                    )
                                }
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className="aboutUs_chooseUs">
                <Container>
                    <Row>
                        <Col md={12} className="text-center">
                            <Title titleLarge="Tại sao chọn Chúng tôi ?"></Title>
                            <span>Giúp doanh nghiệp tối ưu hoạt động quản lý và kinh doanh</span>
                            <div className="aboutUs_chooseUs__list">
                                {
                                    chooseUs_list.map( (item,index) =>
                                        <div className="aboutUs_chooseUs__list__item" key={index}>
                                            <img src={item.img} />
                                            <h5>{item.title}</h5>
                                            <span>{item.text}</span>
                                        </div>
                                    )
                                }
                            </div>
                            <div className="is-line"></div>
                            <Title titleLarge="Tầm nhìn - Sứ mệnh & Giá trị cốt lõi "></Title>
                            <Tabs defaultActiveKey="profile" id="mission-tabs">
                                <Tab eventKey="home" title={<span className="aboutUs_tab"><img src={tab_1}/>Tầm nhìn</span>}>
                                    <div className="mission-tabs_item">
                                        <img src={vision}/>
                                        <p>
                                            Với khát vọng tiên phong trong chiến lược đầu tư, cung cấp giải pháp, sản phẩm, dịch vụ và phát triển bền vững trong lĩnh vực công nghệ thông tin.
                                            <br/>
                                            Công ty công nghệ phần mềm hàng đầu trong lĩnh vực công nghệ thông tin của Việt Nam Iwanna Tech sẽ phấn đấu phát triển theo định hướng nghiên cứu, gia công, thử nghiệm để cung 
                                            cấp các sản phẩm có giá trị đạt chất lượng cao nhằm đáp ứng nhu cầu xã hội góp phần nâng cao chất lượng cuộc sống của người Việt và nâng tầm vị thế của người Việt trên thị trường Quốc tế.
                                        </p>
                                    </div>
                                </Tab>
                                <Tab eventKey="profile" title={<span className="aboutUs_tab"><img src={tab_2}/>Sứ mệnh</span>}>
                                    <div className="mission-tabs_item">
                                        <img src={vision}/>
                                        <p>
                                            Với khát vọng tiên phong trong chiến lược đầu tư, cung cấp giải pháp, sản phẩm, dịch vụ và phát triển bền vững trong lĩnh vực công nghệ thông tin.
                                            <br/>
                                            Công ty công nghệ phần mềm hàng đầu trong lĩnh vực công nghệ thông tin của Việt Nam Iwanna Tech sẽ phấn đấu phát triển theo định hướng nghiên cứu, gia công, thử nghiệm để cung 
                                            cấp các sản phẩm có giá trị đạt chất lượng cao nhằm đáp ứng nhu cầu xã hội góp phần nâng cao chất lượng cuộc sống của người Việt và nâng tầm vị thế của người Việt trên thị trường Quốc tế.
                                        </p>
                                    </div>
                                </Tab>
                                <Tab eventKey="contact" title={<span className="aboutUs_tab"><img src={tab_3}/>Giá trị cốt lõi</span>}>
                                    <div className="mission-tabs_item">
                                        <img src={vision}/>
                                        <p>
                                            Với khát vọng tiên phong trong chiến lược đầu tư, cung cấp giải pháp, sản phẩm, dịch vụ và phát triển bền vững trong lĩnh vực công nghệ thông tin.
                                            <br/>
                                            Công ty công nghệ phần mềm hàng đầu trong lĩnh vực công nghệ thông tin của Việt Nam Iwanna Tech sẽ phấn đấu phát triển theo định hướng nghiên cứu, gia công, thử nghiệm để cung 
                                            cấp các sản phẩm có giá trị đạt chất lượng cao nhằm đáp ứng nhu cầu xã hội góp phần nâng cao chất lượng cuộc sống của người Việt và nâng tầm vị thế của người Việt trên thị trường Quốc tế.
                                        </p>
                                    </div>
                                </Tab>
                            </Tabs>
                        </Col>
                    </Row>
                </Container>
            </div>
        </div>
    );
}

export default About_Us;
