import { Container, Row, Col, Breadcrumb, BreadcrumbItem } from 'react-bootstrap';
import { AiOutlineHome } from "react-icons/ai";

/* Components */
import Title from '../components/Title/Title';
import TitleUnderLine from '../components/Title/TitleUnderLine';
import Services from '../components/Services/Services';
import ContactUs_Box from '../components/ContactUs_Box/ContactUs_Box';

/* Images */
import Service_1 from '../assets/images/services-1.png';
import Service_2 from '../assets/images/services-2.png';
import Service_3 from '../assets/images/services-3.png';
import ServiceUs_bg from '../assets/images/serviceUs_logo.png';

function Service_Us() {

    /* services list text */
    let services_list = [
        {
            id: 1,
            img: `${Service_3}`,
            imgLeft: true,
            titleSmall: 'DỊCH VỤ',
            titleSmall_color: true,
            titleLarge: 'Số Hóa Văn Bản',
            content: [
                'Số hóa hóa đơn, văn bản',
                'Nhận dạng CMND, Passport, Giấy phép lái xe'
            ],
            link: '/service_us/digitalized_text'
        },
        {
            id: 2,
            imgLeft: false,
            img: `${Service_1}`,
            titleSmall: 'DỊCH VỤ',
            titleSmall_color: true,
            titleLarge: 'Giao thông thông minh',
            content: [
                'Nhận diện biển số',
                'Phân loại phương tiện',
                'Đếm lưu lượng phương tiện',
                'Xác định vi phạm: Sai làn, ngược chiều, dừng đỗ, rớt đồ vật trên đường'
            ],
            link: '/service_us/digitalized_text'
        },
        {
            id: 3,
            imgLeft: true,
            img: `${Service_2}`,
            titleSmall: 'DỊCH VỤ',
            titleSmall_color: true,
            titleLarge: 'Phân tích Camera – Face ID',
            content: [
                'Quản lý ra vào bằng khuôn mặt',
                'Điểm danh bằng khuôn mặt',
                'Giám sát khu vực an ninh',
                'Đếm số người'
            ],
            link: '/service_us/digitalized_text'
        }
    ];

    return (
        <div className="wrapper">
            <div className="ServiceUs_breadcrumb">
                <Container>
                    <Row>
                        <Col md={12}>
                            <Breadcrumb>
                                <Breadcrumb.Item href="/"><AiOutlineHome size="24" /></Breadcrumb.Item>
                                <Breadcrumb.Item active>
                                    Dịch vụ - API
                                </Breadcrumb.Item>
                            </Breadcrumb>
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className="home-servicesUs">
                <Container>
                    <Row>
                        <Col md={12} className="text-center" data-aos="fade-up">
                            <TitleUnderLine>Dịch vụ - API</TitleUnderLine>
                        </Col>
                        <Col md={12}>
                            {
                                services_list.map( (service,index) =>  
                                    <Services
                                        key={index}
                                        service={service}
                                        link={service.link}
                                        data_aos="fade-up"
                                    />
                                )
                            }
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className="ServiceUS_brands">
                <Container>
                    <Row>
                        <Col md={12} className="text-center">
                            <span>Được hàng trăm công ty và doanh nghiệp sử dụng trện toàn đất nước</span>
                            <img src={ServiceUs_bg} className="mt-5 mb-5" data-aos="fade-up" />
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className="ServiceUS_contact">
                <Container>
                    <Row>
                        <Col md={12} data-aos="fade-up">
                            <ContactUs_Box/>
                        </Col>
                    </Row>
                </Container>

            </div>
        </div>
    );
}

export default Service_Us;
