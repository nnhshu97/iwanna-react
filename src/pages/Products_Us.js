import { Container, Row, Col, Card, Breadcrumb, BreadcrumbItem } from 'react-bootstrap';
import { AiOutlineHome } from "react-icons/ai";

/* Components */

import ContactUs_Box from '../components/ContactUs_Box/ContactUs_Box';
import TitleUnderLine from '../components/Title/TitleUnderLine';


/* Images */
import ServiceUs_bg from '../assets/images/serviceUs_logo.png';

import Product_1 from '../assets/images/sp-1.png';
import Product_2 from '../assets/images/sp-2.png';

/* Reacr Swiper */
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/swiper.scss';
import 'swiper/components/pagination/pagination.scss';

import '../assets/scss/AboutUs.scss';


/* Use core React Swiper */
SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);

function Products_Us() {
    let title = {
        titleLarge: 'Chúng tôi là ai?'
    };

    /* products list text */
    let products = [
        {
            img: `${Product_1}`,
            title: 'Phần mềm bãi đỗ xe thông minh',
            link: '#'
        },
        {
            img: `${Product_2}`,
            title: 'Phần mềm quản lý trường học và trung tâm giáo dục',
            link: '#'
        }
    ];

    return (
        <div className="wrapper">
            <div className="productsUs_breadcrumb">
                <Container>
                    <Row>
                        <Col md={12}>
                            <Breadcrumb>
                                <Breadcrumb.Item href="/"><AiOutlineHome size="24" /></Breadcrumb.Item>
                                <Breadcrumb.Item active>
                                    Sản phẩm
                                </Breadcrumb.Item>
                            </Breadcrumb>
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className="productsUs">
                <Container>
                    <Row>
                        <Col md={12} className="text-center mb-4">
                            <TitleUnderLine>Sản phẩm</TitleUnderLine>
                        </Col>
                        {
                            products.map( (product,index) => 
                                <Col md={6} key={index}>
                                    <div className="home-productUs-item">
                                        <Card>
                                            <Card.Img variant="top" src={product.img} />
                                            <Card.Body>
                                                <Card.Text className="product-card__subTitle">SẢN PHẨM</Card.Text>
                                                <Card.Title className="product-card__title">{product.title}</Card.Title>
                                                <a href={product.link} className="readMore-link">Tìm hiểu thêm</a>    
                                            </Card.Body>
                                        </Card>
                                    </div>
                                </Col>
                            )
                        }
                    </Row>
                </Container>
            </div>
            <div className="DetailService_brands">
                <Container>
                    <Row>
                        <Col md={12} className="text-center">
                            <span>Được hàng trăm công ty và doanh nghiệp sử dụng trện toàn đất nước</span>
                            <img src={ServiceUs_bg} className="mt-5 mb-5" />
                        </Col>
                    </Row>
                </Container>
            </div>
            <div className="ServiceUS_contact">
                <Container>
                    <Row>
                        <Col md={12}>
                            <ContactUs_Box/>
                        </Col>
                        
                    </Row>
                </Container>
            </div>
        </div>
    );
}

export default Products_Us;
