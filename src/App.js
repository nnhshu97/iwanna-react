import 'bootstrap/dist/css/bootstrap.css';
import './assets/scss/App.scss';

import AOS from "aos";
import "aos/dist/aos.css";

import {
    BrowserRouter as Router,
    Switch,
    Route
} from "react-router-dom";
import HomePage from './pages/HomePage';
import Service_Us from './pages/Service_Us';
import Page from './components/Page/Page';
import About_Us from './pages/About_Us';
import Customers from './pages/Customers';
import Contact_Us from './pages/Contact_Us';
import Digitalized_Text from './pages/Detail_Services/Digitalized_Text';
import Products_Us from './pages/Products_Us';
import Prices from './pages/Prices';
import ScrollToTop from './components/Scroll_To_Top/ScrollToTop';

AOS.init();

function App() {
    return (
        <Router>
            <Page>
                <ScrollToTop />
                <Switch>
                    <Route exact path="/">
                        <HomePage />
                    </Route>
                    <Route exact path="/about_us">
                        <About_Us />
                    </Route>
                    <Route exact path="/customers">
                        <Customers />
                    </Route>
                    <Route exact path="/contact_us">
                        <Contact_Us />
                    </Route>
                    <Route exact path="/service_us">
                        <Service_Us />
                    </Route>
                    <Route exact path="/service_us/digitalized_text">
                        <Digitalized_Text />
                    </Route>
                    <Route exact path="/products_us">
                        <Products_Us />
                    </Route>
                    <Route exact path="/prices">
                        <Prices />
                    </Route>
                </Switch>
            </Page>
        </Router>
    );
}

export default App;
